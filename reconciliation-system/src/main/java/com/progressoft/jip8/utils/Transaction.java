package com.progressoft.jip8.utils;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

@CsvDataType()
public class Transaction {

    @SerializedName("date")
    @CsvField(pos = 6)

    private String date;

    @SerializedName("reference")
    @CsvField(pos = 1)
    private String reference;

    @SerializedName("amount")
    @CsvField(pos = 3)

    private String amount;

    @SerializedName("purpose")
    @CsvField(pos = 5)
    private String purpose;

    @SerializedName("currencyCode")
    @CsvField(pos = 4)

    private String currencyCode;
    @CsvField(pos = 2 )
    private  String transDescription;
    @CsvField(pos =7)
    private  String transtype;

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getReference() {
        return reference;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "date='" + date + '\'' +
                ", reference='" + reference + '\'' +
                ", amount='" + amount + '\'' +
                ", purpose='" + purpose + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", transDescription='" + transDescription + '\'' +
                ", transtype='" + transtype + '\'' +
                '}';
    }

//    public String getTransDescription() {
//        return transDescription;
//    }
//
//    public void setTransDescription(String transDescription) {
//        this.transDescription = transDescription;
//    }
//
//    public String getTranstype() {
//        return transtype;
//    }
//
//    public void setTranstype(String transtype) {
//        this.transtype = transtype;
//    }
}