package com.progressoft.jip8.utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FilesPrinter {
    private StringBuilder matchingFile;
    private StringBuilder misMatchTransaction;
    private StringBuilder missingTransaction;
    public FilesPrinter(){
        matchingFile = new StringBuilder("transaction id,amount,currecny code,value date\n");
        misMatchTransaction = new StringBuilder("found in file,transaction id,amount,currency code,value date\n");
        missingTransaction = new StringBuilder("found in file,transaction id,amount,currency code,value date\n");
    }
    public void buildAndPrint() throws FileNotFoundException {
        fillFiles(matchingFile, misMatchTransaction, missingTransaction);
    }
    public void addTomFile(Transaction transactionObject){
        matchingFile.append(transactionObject.getReference() + "," + transactionObject.getAmount() + "," + transactionObject.getCurrencyCode() + "," + transactionObject.getDate() + "\n");
    }
    public void addTommFile(Transaction source, Transaction target){
        misMatchTransaction.append("SOURCE," + source.getReference() + "," + source.getAmount() + "," + source.getCurrencyCode() + "," + source.getDate() + "\n");
        misMatchTransaction.append("TARGET," + target.getReference() + "," + target.getAmount() + "," + target.getCurrencyCode() + "," + target.getDate() + "\n");

    }
    public void addTomsFile(Transaction transactionObject , boolean source){
        if(source)
            missingTransaction.append("SOURCE,");
        else missingTransaction.append("TARGET,");
         missingTransaction.append(transactionObject.getReference() + "," + transactionObject.getAmount() + "," + transactionObject.getCurrencyCode() + "," + transactionObject.getDate() + "\n");
    }
    private void fillFiles(StringBuilder matchingFile, StringBuilder misMatchTransaction, StringBuilder missingTransaction) throws FileNotFoundException {
        // TODO absolute paths
        printToFile(matchingFile, "src/main/resources/match-file");
        printToFile(misMatchTransaction, "src/main/resources/misMatch-transaction-file");
        printToFile(missingTransaction, "src/main/resources/missing-transaction-file");
    }

    // TODO duplicate code
    private void printToFile(StringBuilder fileContent, String Path) throws FileNotFoundException {
        PrintWriter outputFile = new PrintWriter(Path);
        outputFile.println(fileContent);
        outputFile.close();
    }
}
