package com.progressoft.jip8.utils;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.util.Arrays;

public class JsonFileReader implements FilesReader {

    private Path path;

    public JsonFileReader(Path path) {
        if (path == null)
            throw new IllegalStateException("null json path");
        if (path.toString().equals("document/home"))
            throw new IllegalArgumentException("Json file not exist");
        this.path = path;
    }

    public Transaction[] readFile() throws IOException {
        Gson gson = new Gson();
        Transaction[] transaction;

        try (Reader jsonReader = new FileReader(path.toString())) {

            transaction = gson.fromJson(jsonReader, Transaction[].class);
            for (Transaction transactionIndexError : transaction) {
                jsonTest(transactionIndexError);
            }
            return transaction;
        }

    }

    private void jsonTest(Transaction transactionIndexError) {
        if (!transactionIndexError.getDate().matches("\\d{2}[/]\\d{2}[/]\\d{4}"))
            throw new IllegalStateException("wrong date format in json file");

        Double testingAmountVariable = new Double(transactionIndexError.getAmount());

        if (testingAmountVariable < 0)
            throw new IllegalStateException("Amount have negative value in json file");

        if (!transactionIndexError.getCurrencyCode().matches("[A-Z]{3}"))
            throw new IllegalStateException("currency has wrong format in json file");
    }
}

