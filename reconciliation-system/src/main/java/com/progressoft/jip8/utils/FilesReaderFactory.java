package com.progressoft.jip8.utils;

import java.io.FileNotFoundException;
import java.nio.file.Path;

public class FilesReaderFactory {
    private FilesReaderFactory() {

    }

    public static FilesReader getFileReader(Path path) throws FileNotFoundException {
        if (isJSON(path))
            return new JsonFileReader(path);
        if (isCSV(path))
            return new CSVFilesReader(path);
        throw new IllegalArgumentException("not supported path");
    }

    private static boolean isCSV(Path path) {
        return path.toString().toLowerCase().endsWith(".csv");
    }

    private static boolean isJSON(Path path) {
        return path.toString().toLowerCase().endsWith(".json");
    }
}