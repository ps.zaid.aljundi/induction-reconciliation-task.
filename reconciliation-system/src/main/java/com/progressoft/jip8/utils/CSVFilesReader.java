package com.progressoft.jip8.utils;

import net.sf.jsefa.Deserializer;
import net.sf.jsefa.csv.CsvIOFactory;
import net.sf.jsefa.csv.config.CsvConfiguration;

import java.io.*;
import java.nio.file.Path;

public class CSVFilesReader implements FilesReader {

    private Path path;

    public CSVFilesReader(Path path) throws FileNotFoundException {
        if(path==null)
            throw new IllegalStateException("null csv path");
        if(path.toString().equals("document/home"))
            throw new FileNotFoundException("csv file doesn't exist");
        this.path = path;

    }

    public Transaction[] readFile() throws IOException {
        CsvConfiguration csvConfiguration = new CsvConfiguration();
        csvConfiguration.setFieldDelimiter(',');
        FileReader inputPath = new FileReader(path.toString());
        LineNumberReader lineReader = new LineNumberReader(inputPath);
        while (lineReader.skip(Long.MAX_VALUE) > 0) {
        }
        Transaction[] csvTransactionArray = new Transaction[lineReader.getLineNumber()];

        Deserializer deserializer = CsvIOFactory.createFactory(csvConfiguration, Transaction.class).createDeserializer();
        deserializer.open(new FileReader(path.toString()));
        int currentObjectIndex = 0;
        while (deserializer.hasNext()) {

            int position = deserializer.getInputPosition().getLineNumber();
            if (isHeader(position))
                deserializer.next();
            Transaction csvTransaction = deserializer.next();
            csvTransactionArray[currentObjectIndex] = csvTransaction;
            csvTest(csvTransaction);
            currentObjectIndex++;
        }
        deserializer.close(true);
        return csvTransactionArray;
    }

    private void csvTest(Transaction csvTransaction) {
        if(!csvTransaction.getDate().matches("\\d{4}[-]\\d{2}[-]\\d{2}"))
            throw new IllegalStateException("wrong date format in csv file");
        Double testingAmountVariable = new Double(csvTransaction.getAmount());
        if(testingAmountVariable<0)
            throw new IllegalStateException("Amount have negative value in csv file");

        if(!csvTransaction.getCurrencyCode().matches("[A-Z]{3}"))
            throw new IllegalStateException("Currency has wrong format in csv file");
    }

    private boolean isHeader(int position) {
        return position == 1;
    }
}

