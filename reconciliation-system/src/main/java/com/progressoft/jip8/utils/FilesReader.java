package com.progressoft.jip8.utils;

import java.io.IOException;

public interface FilesReader {
    Transaction[] readFile() throws IOException;
}


