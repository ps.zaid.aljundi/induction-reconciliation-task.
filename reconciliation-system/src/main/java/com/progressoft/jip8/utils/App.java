package com.progressoft.jip8.utils;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Scanner;

public class App {


    public static void main(String[] args) throws IOException, ParseException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter source file location:");
        String sourceFileLocation = scan.nextLine();
//        System.out.println(" Enter source file format:");
//        String sourceFileType = scan.nextLine();
        System.out.println("Enter target file location:");
        String targetFileLocation = scan.nextLine();
//        System.out.println(" Enter target file format:");
//        String targetFileType = scan.nextLine();
        FilesReader firstFileReader = FilesReaderFactory.getFileReader(Paths.get(sourceFileLocation));
        FilesReader secondFileReader = FilesReaderFactory.getFileReader(Paths.get(targetFileLocation));
        Transaction[] firstFileTransactions = firstFileReader.readFile();
        Transaction[] secondFileTransactions = secondFileReader.readFile();

        CompareFile compareFile = new CompareFile();
        compareFile.mapping(firstFileTransactions, secondFileTransactions);
        System.out.println("Reconciliation finished.\n" +
                "Result files are availble in directory src/main/resources\n");
    }
}

