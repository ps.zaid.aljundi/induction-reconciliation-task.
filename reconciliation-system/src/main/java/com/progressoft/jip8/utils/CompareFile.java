package com.progressoft.jip8.utils;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CompareFile {
    // TODO this class do the mapping and writing to files

    // TODO too much long method
    public void mapping(Transaction[] sourceTransactions, Transaction[] targetTransactions) throws FileNotFoundException, ParseException {
        HashMap<String, Boolean> targetFoundFlags = new HashMap<>();
        // TODO too much use of string concatination using builder or write directly to the file using print writer
        FilesPrinter outputFilesBuilder = new FilesPrinter();
        for (Transaction source : sourceTransactions) {
            initSourceFlag(targetFoundFlags, source);
            setDateFormatToCsv(source);
            mismatchingFilePrint(targetTransactions, targetFoundFlags, outputFilesBuilder, source);
            missingSourceFilePrint(targetFoundFlags, outputFilesBuilder, source);
        }
        missingTargetFilePrint(targetTransactions, targetFoundFlags, outputFilesBuilder);
        outputFilesBuilder.buildAndPrint();
    }

    private void missingSourceFilePrint(HashMap<String, Boolean> targetFoundFlags, FilesPrinter outputFilesBuilder, Transaction source) {
        if (!targetFoundFlags.get(source.getReference()))
            outputFilesBuilder.addTomsFile(source, true);
    }

    private void initSourceFlag(HashMap<String, Boolean> targetFoundFlags, Transaction source) {
        if (!targetFoundFlags.containsKey(source.getReference()))
            targetFoundFlags.put(source.getReference(), false);
    }

    private void missingTargetFilePrint(Transaction[] targetTransactions, HashMap<String, Boolean> targetFoundFlags, FilesPrinter outputFilesBuilder) {
        for (Transaction target : targetTransactions){
            if(!targetFoundFlags.containsKey(target.getReference()))
                outputFilesBuilder.addTomsFile(target, false);

        }
    }

    private void mismatchingFilePrint(Transaction[] targetTransactions, HashMap<String, Boolean> targetFoundFlags, FilesPrinter outputFilesBuilder, Transaction source) throws ParseException {
        for (Transaction target : targetTransactions) {
            setDateFormatToCsv(target);
            if (isReferenceMatch(source, target)) {
                if (!isMatched(source, target)) {
                    outputFilesBuilder.addTommFile(source, target);
                    targetFoundFlags.replace(target.getReference(),true);
                } else {
                    outputFilesBuilder.addTomFile(source);
                    targetFoundFlags.replace(target.getReference(), true);
                }
            }
        }
    }

    private boolean isMatched(Transaction source, Transaction target) {

        BigDecimal sourceAmount = getSourceAmount(source);
        BigDecimal targetAmount = getTargetAmount(target);
        return target.getCurrencyCode().equals(source.getCurrencyCode()) && source.getDate().equals(target.getDate()) && isAmountEquals(sourceAmount, targetAmount);

    }

    private void setDateFormatToCsv(Transaction transactionObject) throws ParseException {
        SimpleDateFormat csvDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        transactionObject.setDate(csvDateFormatter.format(getFormatedDate(transactionObject.getDate())));
    }

    private Date getFormatedDate(String date) throws ParseException {
        SimpleDateFormat jsonDateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat csvDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        if (date.charAt(2) == '/') {
            return jsonDateFormatter.parse(date);
        } else {
            return csvDateFormatter.parse(date);
        }
    }

    private boolean isReferenceMatch(Transaction source, Transaction target) {
        return target.getReference().equals(source.getReference());
    }


    private boolean isAmountEquals(BigDecimal sourceAmount, BigDecimal targetAmount) {
        return sourceAmount.toPlainString().equals(targetAmount.toPlainString());
    }

    private BigDecimal getSourceAmount(Transaction source) {
        BigDecimal sourceAmount = new BigDecimal(source.getAmount());
        sourceAmount = sourceAmount.stripTrailingZeros();
        return sourceAmount;
    }

    private BigDecimal getTargetAmount(Transaction target) {
        BigDecimal targetAmount = new BigDecimal(target.getAmount());
        targetAmount = targetAmount.stripTrailingZeros();
        return targetAmount;
    }
}

