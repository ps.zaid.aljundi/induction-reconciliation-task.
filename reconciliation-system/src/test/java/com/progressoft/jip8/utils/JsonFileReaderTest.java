package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonFileReaderTest{
    Path path;

    @Test
    public void givenNullPath_whenConstructing_thenThrowIllegalStateException() {
        path = null;
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> new JsonFileReader(path));
        Assertions.assertEquals("null json path", exception.getMessage());
    }
    @Test
    public void givenInvalidPath_whenConstructing_thenThrowIllegalStateException() {
        path = Paths.get("document/home");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new JsonFileReader(path));
        Assertions.assertEquals("Json file not exist", exception.getMessage());
    }
    @Test
    public void givenInvalidDateFormatInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("/home/user/task/induction-reconciliation-task/reconciliation-system/src/test/resources/online-banking-transactionsDateDebug.json");
        JsonFileReader jsonReader = new JsonFileReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, jsonReader::readFile);
        Assertions.assertEquals("wrong date format in json file", illegalStateException.getMessage());
    }
    @Test
    public void givenNegativeAmountInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("/home/user/task/induction-reconciliation-task/reconciliation-system/src/test/resources/online-banking-transactionsAmountDebug.json");
        JsonFileReader jsonReader = new JsonFileReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, jsonReader::readFile);
        Assertions.assertEquals("Amount have negative value in json file", illegalStateException.getMessage());
    }
    @Test
    public void givenInvalidMoneyFormatInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("/home/user/task/induction-reconciliation-task/reconciliation-system/src/test/resources/online-banking-transactionsCurrencyDebug.json");
        JsonFileReader jsonReader = new JsonFileReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, jsonReader::readFile);
        Assertions.assertEquals("currency has wrong format in json file", illegalStateException.getMessage());
    }
}