package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVFilesReaderTest {
    Path path;

    @Test
    public void givenNullPath_whenConstructing_thenThrowIllegalStateException() {
        path = null;
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> new CSVFilesReader(path));
        Assertions.assertEquals("null csv path", exception.getMessage());
    }
    @Test
    public void givenInvalidPath_whenConstructing_thenThrowFileNotFoundException() {
        path = Paths.get("document/home");
        FileNotFoundException exception = Assertions.assertThrows(FileNotFoundException.class, () -> new CSVFilesReader(path));
        Assertions.assertEquals("csv file doesn't exist", exception.getMessage());
    }
    @Test
    public void givenInvalidDateFormatInsideCSVFile_whenReading_thenThrowIllegalStateException() throws FileNotFoundException {
        path = Paths.get("/home/user/task/induction-reconciliation-task/reconciliation-system/src/test/resources/bank-transactionsDateDebug.csv");
        CSVFilesReader csvReader = new CSVFilesReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::readFile);
        Assertions.assertEquals("wrong date format in csv file", illegalStateException.getMessage());
    }
    @Test
    public void givenNegativeAmountInsideCSVFile_whenReading_thenThrowIllegalStateException() throws FileNotFoundException {
        path = Paths.get("/home/user/task/induction-reconciliation-task/reconciliation-system/src/test/resources/bank-transactionsAmountDebug.csv");
        CSVFilesReader csvReader = new CSVFilesReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::readFile);
        Assertions.assertEquals("Amount have negative value in csv file", illegalStateException.getMessage());
    }
    @Test
    public void givenInvalidMoneyFormatInsideFile_whenReading_thenThrowIllegalStateException() throws FileNotFoundException {
        path = Paths.get("/home/user/task/induction-reconciliation-task/reconciliation-system/src/test/resources/bank-transactionsMoneyDebug.csv");
        CSVFilesReader csvReader = new CSVFilesReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::readFile);
        Assertions.assertEquals("Currency has wrong format in csv file", illegalStateException.getMessage());
    }

}
