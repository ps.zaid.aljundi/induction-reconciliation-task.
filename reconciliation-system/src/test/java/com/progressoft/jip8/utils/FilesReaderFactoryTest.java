package com.progressoft.jip8.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FilesReaderFactoryTest {
    Path path ;
    @Test
    public void givenNullPath_whenConstructing_thenThrowIllegalStateException() {
        path =Paths.get("abc.abc") ;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> FilesReaderFactory.getFileReader(path));
        Assertions.assertEquals("not supported path", exception.getMessage());
    }
}
